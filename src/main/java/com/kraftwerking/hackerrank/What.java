package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class What {

	public static void main(String[] args) throws IOException {
		/*
		 * Enter your code here. Read input from STDIN. Print output to STDOUT.
		 * Your class should be named Solution.
		 */
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	    String line;

	    line = br.readLine();
	    int pos = Integer.parseInt(line);

	    line = br.readLine();
	    String[] numbers = line.split("\\s+");

	    if ((numbers.length < pos) || (pos < 0)) {
	        System.out.println("NIL");
	    }
	    else {
	        System.out.println(numbers[numbers.length-pos]);
	    }
	    

	}

}