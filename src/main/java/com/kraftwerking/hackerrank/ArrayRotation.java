package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class ArrayRotation {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();//size of int array
        int k = in.nextInt();//num of rotation
        int q = in.nextInt();//num of questions m
        int[] a = new int[n];
        for(int a_i=0; a_i < n; a_i++){
            a[a_i] = in.nextInt();
        }
        
        //rotate array k times
        int[] tmp = a.clone();
        for(int i=0;i<n;i++){
        	//System.out.println((i+k)%n);
        	tmp[(i+k)%n]=a[i];
        	
        }
            
        //asking questions
        for(int a0 = 0; a0 < q; a0++){
            int m = in.nextInt();
            System.out.println(tmp[m]);
        }
        

    }
}
