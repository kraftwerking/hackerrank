package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class DesignerPdfViewer {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = 26;
		int h[] = new int[n];
		char c[] = { 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't',
				'u', 'v', 'w', 'x', 'y', 'z' };
		for (int h_i = 0; h_i < n; h_i++) {
			h[h_i] = in.nextInt();
		}
		
		Map<Character, Integer> map = new HashMap<Character, Integer>();
		for (int i = 0; i < c.length; i++) {
			map.put(c[i], h[i]);
		}

		String word = in.next();
		int ret = 0;
		for(int i=0;i<word.length();i++){
			int tm = map.get(word.charAt(i));
			if(tm>ret){
				ret=tm;
			}

		}
		ret = ret * (1*word.length());
		System.out.println(ret);
	}
}
