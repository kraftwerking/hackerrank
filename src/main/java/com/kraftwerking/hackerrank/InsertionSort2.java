package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;

public class InsertionSort2 {

	public static void insertionSortPart2(int[] ar) {
		// Fill up the code for the required logic here
		// Manipulate the array as required
		// The code for Input/Output is already provided

		for (int i = 0; i < ar.length - 1; i++) {
			// System.out.println(i + " " + ar[i]);
			if (ar[i] > ar[i + 1]) {
				// System.out.println("move " + ar[i+1]);
				// System.out.println("");
				int[] tmp = move(ar, ar[i + 1]);
				// printArray(tmp);
			}
			printArray(ar);
		}

	}

	private static int[] move(int[] ar, int mv) {
		// TODO Auto-generated method stub
		ArrayList<Integer> newAr = new ArrayList<Integer>();
		for (int i = 0; i < ar.length; i++) {
			if (ar[i] < mv) {
				newAr.add(ar[i]);
				break;
			}
		}
		newAr.add(mv);
		// System.out.println(newAr.toString());
		for (int i = 0; i < ar.length; i++) {
			if (!newAr.contains(ar[i])) {
				newAr.add(ar[i]);
			}
		}
		// System.out.println(newAr.toString());
		for (int i = 0; i < ar.length; i++) {
			ar[i] = newAr.get(i);
		}
		return ar;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int s = in.nextInt();
		int[] ar = new int[s];
		for (int i = 0; i < s; i++) {
			ar[i] = in.nextInt();
		}
		insertionSortPart2(ar);

	}

	private static void printArray(int[] ar) {
		for (int n : ar) {
			System.out.print(n + " ");
		}
		System.out.println("");
	}
}
