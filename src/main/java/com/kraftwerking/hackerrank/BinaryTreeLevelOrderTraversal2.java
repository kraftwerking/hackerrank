package com.kraftwerking.hackerrank;

import java.util.*;
import java.io.*;

class Nodeee {
	Nodeee left, right;
	int data;

	Nodeee(int data) {
		this.data = data;
		left = right = null;
	}
}

class BinaryTreeLevelOrderTraversal2 {

   /* 
    
    class Nodeee 
       int data;
       Nodeee left;
       Nodeee right;
   */
   void LevelOrder(Nodeee root)
    {
       int h = getHt(root);
       //System.out.println(h);
       System.out.print(root.data + " ");

       for(int i=1;i<=h;i++){
           printLevel(root.left, i);
           printLevel(root.right, i);
       }
      
      
    }
	static int getHt(Nodeee root){

		if(root==null){
			return -1;
		} else {
			int lht = getHt(root.left);
			int rht = getHt(root.right);
			if(lht>rht){
				return lht+1;
			} else {
				return rht+1;
			}
			
		}

	}

    void printLevel(Nodeee root, int level){
        if(root==null){
            return;
        }
        if(level==1){
            System.out.print(root.data + " ");
        } else if(level>1){
            printLevel(root.left,level-1);
            printLevel(root.right,level-1);
        }
        
    }
}