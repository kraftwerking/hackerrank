package com.kraftwerking.hackerrank;

import java.util.*;
import java.io.*;

class Noodddd {
	Noodddd left, right;
	int data;

	Noodddd(int data) {
		this.data = data;
		left = right = null;
	}
}

class BinaryTreeHeight2 {

	/*
    class Noodddd 
    	int data;
    	Noodddd left;
    	Noodddd right;
	*/
static int getHeight(Noodddd root) {
		// Write your code here.
		return getHt(root);
	}
	
	static int getHt(Noodddd root){

		if(root==null){
			return -1;
		} else {
			int lht = getHt(root.left);
			int rht = getHt(root.right);
			if(lht>rht){
				return lht+1;
			} else {
				return rht+1;
			}
			
		}

	}

	public static Noodddd insert(Noodddd root, int data) {
		if (root == null) {
			return new Noodddd(data);
		} else {
			Noodddd cur;
			if (data <= root.data) {
				cur = insert(root.left, data);
				root.left = cur;
			} else {
				cur = insert(root.right, data);
				root.right = cur;
			}
			return root;
		}
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		Noodddd root = null;
		while (T-- > 0) {
			int data = sc.nextInt();
			root = insert(root, data);
		}
		int height = getHeight(root);
		System.out.println(height);
	}
}