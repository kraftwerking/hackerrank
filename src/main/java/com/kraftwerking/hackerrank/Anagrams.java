package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Anagrams {
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);

		String str = in.next();
		String str2 = in.next();
		char[] arr = str.toCharArray();
		char[] arr2 = str2.toCharArray();
		int count = 0;
		
		ArrayList<Character> chars = new ArrayList<Character>();
		for (char c : str.toCharArray()) {
		  chars.add(c);
		}		
		ArrayList<Character> chars2 = new ArrayList<Character>();
		for (char c : str2.toCharArray()) {
		  chars2.add(c);
		}		
		
		for(Iterator<Character> i = chars.iterator(); i.hasNext(); ) {
		    Character ch = i.next();
		    if(!chars2.contains(ch)){
		    	i.remove();
		    	count++;
		    }
		}
		
		for(Iterator<Character> i = chars2.iterator(); i.hasNext(); ) {
		    Character ch = i.next();
		    if(!chars.contains(ch)){
		    	i.remove();
		    	count++;
		    }
		}

		System.out.println(count);

	}
}