package com.kraftwerking.hackerrank;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class RegexPatterns {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int N = in.nextInt();

        List<String> al = new ArrayList<String>();
        for(int a0 = 0; a0 < N; a0++){
            String firstName = in.next();
            String emailID = in.next();
            String gm = "@gmail.com";

            if(emailID.toLowerCase().contains(gm.toLowerCase())) {
                al.add(firstName);
            }

        }
        al.sort(String::compareToIgnoreCase);
        for(String s:al){
            System.out.println(s);
        }


    }

}

