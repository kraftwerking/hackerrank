package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class StringRotation {

	public static void main(String[] args) {
		String[][] pairs = {{"apple", "pleap"}, {"waterbottle", "erbottlewat"}, {"camera", "macera"}};
		for (String[] pair : pairs) {
			String word1 = pair[0];
			String word2 = pair[1];
			boolean is_rotation = isRotation(word1, word2);
			System.out.println(word1 + ", " + word2 + ": " + is_rotation);
		}
	}

	private static boolean isRotation(String word1, String word2) {
		// TODO Auto-generated method stub
		boolean ret = false;
		String ne = word1;
		int spl=0;
		for(int i=0;i<word2.length();i++){
			ne = removeFirstChar(ne);
			//System.out.println(ne);
			if(ne.length()==1){
				break;
			} else {
				if(isSubstring(ne,word2)){
					ret=true;
					spl=i;
					break;
				}
			}
		}
		
		if(ret==true){
			System.out.println("ret=true " + spl);
			String tm1 = word1.substring(0, spl+1);
			String tm2 = word1.substring(spl+1, word1.length());
			System.out.println(tm1 + " " + tm2);
			String tm3=tm2+tm1;
			if(!(tm3.equals(word2))){
				ret=false;
				
			}
		}

		
		return ret;
	}
	
	private static boolean isSubstring(String ne, String word2) {
		// TODO Auto-generated method stub
		if (word2.contains(ne)){
			return true;	
		} else {
			return false;
		}

	}

	public static String removeFirstChar(String s){
		   return s.substring(1);
		}
}