package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class LonelyInteger {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int a[] = new int[n];
        for(int a_i=0; a_i < n; a_i++){
            a[a_i] = in.nextInt();
        }
        
        //convert to arr list
        List<Integer> l = new ArrayList<Integer>();
        for(int intValue : a) {
            l.add(intValue);
        }
    	HashSet<Integer> set = new HashSet<>();
        List<Integer> res = new ArrayList<Integer>();

        //loop
        for(Integer i:l){
        	if(!set.contains(i)){
        		set.add(i);
        		res.add(i);
        	} else {
        		res.remove(i);
        	}
        }
        
        //look at resulting arr list
        for(Integer i:res){
        	System.out.print(i);
        }
        
    }
}
