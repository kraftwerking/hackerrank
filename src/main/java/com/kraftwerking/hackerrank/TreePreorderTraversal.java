package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class TreePreorderTraversal {

	/* you only have to complete the function given below.  
	Node is defined as  

	class Node {
	    int data;
	    Node left;
	    Node right;
	}

	*/
	class Node {
	    int data;
	    Node left;
	    Node right;
	}
	void preOrder(Node root) {
	    Node tm = root;
	    if(tm!=null){
	        System.out.print(tm.data + " ");
	        preOrder(tm.left);
	        preOrder(tm.right);    
	    }
	}

}
