package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

import com.kraftwerking.hackerrank.StackMaxVal.StackNode;

public class QueueTest {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		Queue q = new Queue();
		
		while (n > 0) {
			int choice = in.nextInt();
			if (choice == 1) {
				int val = in.nextInt();
				q.add(val);
				
			} else if (choice == 2) {
				if(!q.isEmpty()){
					int tm = q.remove();			
				}


			} else if (choice == 3) {
				System.out.println(q.peek());
			}

			n--;
		}
		in.close();

	}

	private static class Node {
		private int data;
		private Node next;

		private Node(int data) {
			this.data = data;

		}
	}
	public static class Queue {

		private Node head;// remove here fifo
		private Node tail; // add here

		public boolean isEmpty() {
			return head == null;

		}

		public int peek() {
			if (head != null) {
				return head.data;
			}
			return -1;

		}

		public void add(int data) {
			Node node = new Node(data);
			if (head == null) {
				head = node;
			}
			if (tail != null) {
				tail.next = node;
			}
			tail = node;
		}

		public int remove() {
			int data = head.data;
			head = head.next;
			if (head == null) {
				tail = null;
			}
			return data;
		}

	}

}
