package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class DiagonalDifference {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		in.nextLine();
		int sum =0;
		int sum2 =0;
		int start=n-1;
		for(int i=0;i<n;i++){
			String[] str = in.nextLine().split(" ");
			//System.out.println("str " + str[i]);
			sum+=Integer.parseInt(str[i]);
			//System.out.println(str[start-i]);
			sum2+=Integer.parseInt(str[start-i]);

		}

		in.close();
		System.out.println(Math.abs(sum-sum2));

	}

}
