package com.kraftwerking.hackerrank;

import java.util.*;
import java.io.*;

class Noodd {
	int data;
	Noodd left;
	Noodd right;
	public Noodd(int data, Noodd left, Noodd right) {
		super();
		this.data = data;
		this.left = left;
		this.right = right;
	}
	public Noodd() {
		super();
		this.data = 0;
		this.left = null;
		this.right = null;
	}
	
}

class BinaryTreePreOrderTraversal {
	/*
	 * 
	 * class Noodd int data; Noodd left; Noodd right;
	 */
	 /* Noodd is defined as :
	 class Noodd 
	    int data;
	    Noodd left;
	    Noodd right;
	    
	    */


void preOrder(Noodd root) {
    if(root!=null){
        printPre(root);
    }
}

void printPre(Noodd root){
    if(root!=null){
        System.out.print(root.data + " ");
        if(root.left!=null){
            printPre(root.left);
        }
        if(root.right!=null){
            printPre(root.right);
        }
    }
}

}
