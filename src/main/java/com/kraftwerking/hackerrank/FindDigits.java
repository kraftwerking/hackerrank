package com.kraftwerking.hackerrank;

import java.util.Scanner;

public class FindDigits {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int t = in.nextInt();
        for(int i=0;i<t;i++){
            String n = in.next();
            //System.out.println(n);
            int x = Integer.parseInt(n);
            int res = 0;
            int[] num = new int[n.length()];

            for (int j = 0; j < n.length(); j++){
                num[j] = n.charAt(j) - '0';
            }

            for (int k : num) {
                //System.out.println(k);
                if(k!=0){
                    if(x%k==0){
                        res++;
                    }
                }

            }
            System.out.println(res);
        }



        in.close();
        //System.out.println();
    }
}
