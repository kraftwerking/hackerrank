package com.kraftwerking.hackerrank;

import java.util.Scanner;

public class JumpingOnTheClouds {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int c[] = new int[n];
        for(int c_i=0; c_i < n; c_i++){
            c[c_i] = in.nextInt();
        }

        int e =100;
        for(int on=0;on<n;on=on+k){
            //System.out.println(on);
            if(c[on]==1){
                //System.out.println("thundercloud");
                e=e-2;
            }
            e=e-1;
        }

        in.close();
        System.out.println(e);
    }
}
