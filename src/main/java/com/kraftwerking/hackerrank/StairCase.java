package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class StairCase {
	/*
	 * Complete the function below.
	 */

	static void StairCase(int n) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < n; i++) {
			for(int j=1;j<=n;j++){
				if(j>=n-i){
					sb.append("#");
				} else {
					sb.append(" ");

				}

			}
			sb.append("\n");

		}
		System.out.print(sb.toString());
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int _n;
		_n = Integer.parseInt(in.nextLine().trim());

		StairCase(_n);

	}
}