package com.kraftwerking.hackerrank;

import java.util.*;

class Personx {
	protected String firstName;
	protected String lastName;
	protected int idNumber;
	
	// Constructor
	Personx(String firstName, String lastName, int identification){
		this.firstName = firstName;
		this.lastName = lastName;
		this.idNumber = identification;
	}
	
	// Print person data
	public void printPersonx(){
		 System.out.println(
				"Name: " + lastName + ", " + firstName 
			+ 	"\nID: " + idNumber); 
	}
	 
}

class Student extends Personx{
	public Student(String firstName, String lastName, int id, int[] testScores2) {
		// TODO Auto-generated constructor stub
		super(firstName, lastName, id);
		this.testScores=testScores2;
	}

	public char calculate(){
		int grade = 0;
		char ret = 0;
		for(int i=0;i<this.testScores.length;i++){
			grade+=this.testScores[i];
		}
		grade=grade/this.testScores.length;
		
		if(grade>=90 && grade <=100){
			ret='O';
		} else if(grade>=80 && grade <90){
			ret='E';
		} else if(grade>=70 && grade <80){
			ret='A';
		}  else if(grade>=55 && grade <70){
			ret='P';
		}  else if(grade>=40 && grade <55){
			ret='D';
		} else if(grade<40){
			ret='T';
		}
		
		
		
		return ret;
	}
	
	private int[] testScores;
   
}

class Inheritence {
	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		String firstName = scan.next();
		String lastName = scan.next();
		int id = scan.nextInt();
		int numScores = scan.nextInt();
		int[] testScores = new int[numScores];
		for(int i = 0; i < numScores; i++){
			testScores[i] = scan.nextInt();
		}
		scan.close();
		
		Student s = new Student(firstName, lastName, id, testScores);
		s.printPersonx();
		System.out.println("Grade: " + s.calculate());
	}
}