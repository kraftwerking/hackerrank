package com.kraftwerking.hackerrank;

import java.util.*;
import java.io.*;

class Ndooe {
	Ndooe left, right;
	int data;

	Ndooe(int data) {
		this.data = data;
		left = right = null;
	}
}

class BinaryTreeLowestCommonAncestor2 {

	/*
	 * Ndooe is defined as : class Ndooe int data; Ndooe left; Ndooe right;
	 * 
	 */

	static Ndooe lca(Ndooe root,int v1,int v2)
	{
	    //Decide if you have to call rekursively
	    //Samller than both
	    if(root.data < v1 && root.data < v2){
	        return lca(root.right,v1,v2);
	    }
	    //Bigger than both
	    if(root.data > v1 && root.data > v2){
	        return lca(root.left,v1,v2);
	    }

	    //Else solution already found
	    return root;
	}

}