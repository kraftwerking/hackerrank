package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class HackerlandRadio {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        Integer[] x = new Integer[n];
        for(int x_i=0; x_i < n; x_i++){
            x[x_i] = in.nextInt();
        }
        List<Integer> intList = Arrays.asList(x);
        Collections.sort(intList);
        //System.out.println(x[x.length-1]-x[0]+1);
        
        int town = x[x.length-1]-x[0]+1;
        int rg = 1+(2*k);
        int mo = town%rg;
        double how = Math.ceil((double)town/rg);
        System.out.println((int)how);

    }
}
