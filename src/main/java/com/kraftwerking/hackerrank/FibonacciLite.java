package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class FibonacciLite {

    public static void main(String[] args) throws IOException {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	    String line;

	    line = br.readLine();
	    int n = Integer.parseInt(line);

	    System.out.print(fibonacci(n));
    
    }

	private static int fibonacci(int i) {
		// TODO Auto-generated method stub
		if(i==0){
			return 0;
		} else if (i==1){
			return 1;
		}
		
		return fibonacci(i-1) + fibonacci(i-2);
	}


}