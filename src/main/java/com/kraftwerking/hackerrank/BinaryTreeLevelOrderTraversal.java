package com.kraftwerking.hackerrank;

import java.util.*;
import java.io.*;

class Noode{
	int data;
	Noode left;
	Noode right;
}
class BinaryTreeLevelOrderTraversal {
   /* 
    
    class Noode 
       int data;
       Noode left;
       Noode right;
   */
   void LevelOrder(Noode root)
    {
      int h = height(root);
      //System.out.println(h);
      for(int i=1;i<=h;i++){
          printLevel(root,i);
      }
       
    }
    int height(Noode root){
        if(root==null){
            return 0;
        } else {
            int lht = (height(root.left));
            int rht = (height(root.right));
            if(lht>rht){
                return lht + 1;
            } else {
                return rht + 1;
            }
        }
    }
    void printLevel(Noode root,int h){
        //System.out.println(h);
        if(root==null){
            return;
        } else {
            if(h==1){
                System.out.print(root.data + " ");
            } else if ( h>1){
                printLevel(root.left,h-1);
                printLevel(root.right,h-1);
            }
            
        }
    }

}