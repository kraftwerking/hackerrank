package com.kraftwerking.hackerrank;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

public class SimpleTextEditor {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		Stack s = new Stack();
		Stack und = new Stack();
		
		while (n > 0) {
			int choice = in.nextInt();
			if (choice == 1) {
				String str = in.next();
				//System.out.println(str);
				String st = 1 + " " + str + ",";
				und.push(st);
				s.push(str);
				
			} else if (choice == 2) {
				int tm = in.nextInt();
				//System.out.println(tm);
				String st = 2 + " " + tm + s.peek()+ ",";
				und.push(st);
				String x = s.delChar(tm);
				
			} else if (choice == 3) {
				int tm = in.nextInt();
				//System.out.println(tm);
				System.out.println(s.printChar(tm));
				
			} else if (choice == 4) {
				//System.out.println("Undo");
				//System.out.println(und.pop());
				// ex. 1 abc,2 3,1 xy,
				String str = und.pop();
				List<String> elephantList = Arrays.asList(str.split(","));
				String cmd = elephantList.get(elephantList.size()-1);
				//System.out.println(elephantList);
				//System.out.println(cmd);
				elephantList = Arrays.asList(cmd.split(" "));
				if(elephantList.get(0).equals("1")){
					int tm = elephantList.get(1).length();
					String x = s.delChar(tm);
				} else if(elephantList.get(0).equals("2")){
					String sr = elephantList.get(1).substring(1);
					s.push(sr);
				}
				
			}

			n--;
		}
		in.close();

	}

	private static class Node {
		private String data;
		private Node next;

		private Node(String data) {
			this.data = data;

		}
	}
	public static class Stack {

		private Node top;// only point of insertion

		public boolean isEmpty() {
			return top == null;

		}

		public String peek() {
			if (top != null) {
				return top.data;
			}
			return "";

		}

		public void push(String data) {
			if(top!=null){
				data = top.data+data;
			}
			Node node = new Node(data);
			node.next=top;
			top = node;
		}

		public String pop() {
			String data = top.data;
			if(top.next!=null){
				top = top.next;				
			} else {
				top =null;
			}

			return data;
		}
		
		public char printChar(int pos) {
			String data = top.data;
			char c = data.charAt(pos-1);
			return c;
		}
		
		public String delChar(int pos) {
			String data = top.data;
			String str = data.substring(0,data.length()-pos);
			top.data=str;
			return str;
		}

	}

}
