	package com.kraftwerking.hackerrank;
	
	import java.io.*;
	import java.util.*;
	import java.text.*;
	import java.math.*;
	import java.util.regex.*;
	
	public class FunnyString {
	
	    public static void main(String[] args) {
	        Scanner in = new Scanner(System.in);
	        int t = in.nextInt();
	        String s = new String();
	        in.nextLine();

	        for(int i=0; i < t; i++){
		        s = in.nextLine();	        
		        if(isFunny(s)){
		        	System.out.println("Funny");
		        } else {
		        	System.out.println("Not Funny");
     	
		        }
	        }
	    }

		private static boolean isFunny(String s) {
			// TODO Auto-generated method stub
	        String r = new StringBuilder(s).reverse().toString();	
	        boolean funny = true;
	        for(int i=1;i<s.length();i++){
	        	int si = (int) s.charAt(i);
	        	int simin1 = (int) s.charAt(i-1);
	        	int ri = (int) r.charAt(i);
	        	int rimin1 = (int) r.charAt(i-1);
	        	//System.out.println(si + " " + simin1 + " " + ri + " " + rimin1);
	        	
	        	if(Math.abs(si-simin1)!=Math.abs(ri-rimin1)){
	        		funny=false;
	        		return funny;
	        	} 
	        }	        
			return funny;
		}
	}
