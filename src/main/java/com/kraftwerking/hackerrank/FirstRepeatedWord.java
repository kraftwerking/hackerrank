package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class FirstRepeatedWord {

	/*
	 * Complete the function below.
	 */

	static String firstRepeatedWord(String s) {
		String[] words = s.split("\\W+");

		for (int j = 0; j < words.length; j++) {
			for (int k = j + 1; k < words.length; k++) {


				if (k != j && words[k].equals(words[j])) {
					return words[k];
				}
			}
		}

		return "none";
	}

	public static void main(String[] args) throws IOException {
		String s = "He had had quite enough of this nonsense";
		System.out.println(firstRepeatedWord(s));
	}
}