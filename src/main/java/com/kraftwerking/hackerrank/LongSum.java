package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class LongSum {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		in.nextLine();
		String s = in.nextLine();
		long[] arr = new long[n];
		String[] sarr = s.split("\\s+");
		long ret = 0;

		for (int i = 0; i < sarr.length; i++) {
			// System.out.println(sarr[i]);

			arr[i] = Integer.parseInt(sarr[i]);

		}
		for (Long a : arr) {
			ret += a;
		}

		System.out.println(ret);
	}

}
