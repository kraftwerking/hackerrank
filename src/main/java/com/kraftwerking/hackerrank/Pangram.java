package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;

public class Pangram {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
		Scanner in = new Scanner(System.in);
		String s = in.nextLine().toLowerCase();
		char[]alphabet={'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z'};
		for(int i=0;i<alphabet.length;i++){
			char c = alphabet[i];
			if(!s.contains(String.valueOf(c))){
				System.out.println("not pangram");
				System.exit(1);
			}
		}
		
		in.close();
		System.out.println("pangram");
    }
}