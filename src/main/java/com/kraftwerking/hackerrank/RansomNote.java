package com.kraftwerking.hackerrank;

import java.util.*;

public class RansomNote {
    Map<String, Integer> magazineMap;
    Map<String, Integer> noteMap;
    
    public RansomNote(String magazine, String note) {
    	//System.out.println(magazine);
    	//System.out.println(note);
    	final String[] f = magazine.split(" ");
    	final String[] s = note.split(" ");
    	magazineMap = new  HashMap<String, Integer>();
    	noteMap = new  HashMap<String, Integer>();
    	
    	for(String i:f){
    		if (magazineMap.containsKey(i)) {
    			magazineMap.put(i, magazineMap.get(i) + 1);
    		} else {
    			magazineMap.put(i, 1);
    		}
    	}
    	for(String i:s){
    		if (noteMap.containsKey(i)) {
    			noteMap.put(i, noteMap.get(i) + 1);
    		} else {
    			noteMap.put(i, 1);
    		}
    	}

    }
    
    public boolean solve() {
    	
    	boolean ret = true;
    	
    	 Iterator it = noteMap.entrySet().iterator();
    	    while (it.hasNext()) {
    	        Map.Entry pair = (Map.Entry)it.next();
    	        //System.out.println(pair.getKey() + " = " + pair.getValue());
    	        if(magazineMap.containsKey(pair.getKey())){
    	        	if(magazineMap.get(pair.getKey()) != pair.getValue()){
    	        		ret=false;
    	        		break;
    	        	}
    	        }
    	        
    	        it.remove(); // avoids a ConcurrentModificationException
    	    }
    	
        return ret;
    }

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        int m = scanner.nextInt();
        int n = scanner.nextInt();
        
        // Eat whitespace to beginning of next line
        scanner.nextLine();
        
        RansomNote s = new RansomNote(scanner.nextLine(), scanner.nextLine());
        scanner.close();
        
        boolean answer = s.solve();
        if(answer)
            System.out.println("Yes");
        else System.out.println("No");
      
    }
}
