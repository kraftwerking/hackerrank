package com.kraftwerking.hackerrank;

import com.kraftwerking.hackerrank.CtCILibrary.*;

//Complete this code or write your own from scratch
import java.util.*;
import java.io.*;


class StackMinVal {

    public static void main(String[] args) {
        StackMinVal s1 = new StackMinVal();
        s1.push(3);
        s1.push(6);
        System.out.println(s1.getMin() == 3);
        System.out.println(s1.pop() == 6);
        System.out.println(s1.getMin() == 3);
        System.out.println(s1.pop() == 3);
        System.out.println(s1.pop() == -1);
    }

    private NodeInteger top;

    public void push(int val) {
        NodeInteger n = new NodeInteger(val);
        if (top == null) {
	    n.setMin(val);
        } else if (val < top.getMin()) {
	    n.setMin(val);
        } else {
	    n.setMin(top.getMin());
        }
	n.setNext(top);
	top = n;
    }

    public int pop() {
        if (top == null) {
            return -1;
        } else {
            int val = top.getValue();
            top = top.getNext();
            return val;
        }
    }

    public int getMin() {
        if (top == null) {
            return -1;
        } else {
            return top.getMin();
        }
    }
    
    public class NodeInteger {
        NodeInteger next;
        NodeInteger prior;
        int value;
        int min;

        public NodeInteger(int value) {
            this.value = value;
        }

        public void setNext(NodeInteger next) {
            this.next = next;
        }
        public void setPrior(NodeInteger prior) {
    	this.prior = prior;
        }
        public void setMin(int val) {
    	this.min = val;
        }
        public int getMin() {
    	return this.min;
        }
        public void setValue(int value) {
    	this.value = value;
        }
        public int getValue() {
    	return this.value;
        }
        public NodeInteger getNext() {
            return this.next;
        }
        public NodeInteger getPrior() {
            return this.prior;
        }
    }

}