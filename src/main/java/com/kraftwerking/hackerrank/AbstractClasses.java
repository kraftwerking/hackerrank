package com.kraftwerking.hackerrank;

import java.util.*;
abstract class Booku
{
    String title;
    String author;
    Booku(String t,String a){
        title=t;
        author=a;
    }
    abstract void display();


}

//Write MyBooku Class
class MyBooku extends Booku{

	int price;
	MyBooku(String t, String a, int p) {
		super(t, a);
		// TODO Auto-generated constructor stub
		price=p;
	}

	@Override
	void display() {
		// TODO Auto-generated method stub
		System.out.println("Title: " + this.title);
		System.out.println("Author: " + this.author);
		System.out.println("Price: " + this.price);
		
	}
	
}


public class AbstractClasses
{
   
   public static void main(String []args)
   {
      Scanner sc=new Scanner(System.in);
      String title=sc.nextLine();
      String author=sc.nextLine();
      int price=sc.nextInt();
      Booku new_novel=new MyBooku(title,author,price);
      new_novel.display();
      
   }
}
