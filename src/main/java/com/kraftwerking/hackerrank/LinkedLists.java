package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;

class Nidididoode {
	int data;
	Nidididoode next;

	Nidididoode(int d) {
		data = d;
		next = null;
	}
}

class LinkedLists {

	public static Nidididoode insert(Nidididoode head, int data) {
		// Complete this method
		Nidididoode n = new Nidididoode(data);
		if(head==null){
			
			head=n;

		} else{
			//System.out.println(head.data);

			Nidididoode start = head;
			while (start.next != null) {
				//System.out.print(start.data + " ");
				start = start.next;
			}
			start.next=n;
		}
		
		return head;
	}

	public static void display(Nidididoode head) {
		Nidididoode start = head;
		while (start != null) {
			System.out.print(start.data + " ");
			start = start.next;
		}
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		Nidididoode head = null;
		int N = sc.nextInt();

		while (N-- > 0) {
			int ele = sc.nextInt();
			head = insert(head, ele);
		}
		display(head);
		sc.close();
	}
}