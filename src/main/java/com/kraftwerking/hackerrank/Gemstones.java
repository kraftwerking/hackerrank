	package com.kraftwerking.hackerrank;
	
	import java.io.*;
	import java.util.*;
	import java.text.*;
	import java.math.*;
	import java.util.regex.*;
		
	public class Gemstones {
	
	    public static void main(String[] args) {
	        Scanner in = new Scanner(System.in);
	        int t = in.nextInt();
	        String s = new String();
	        in.nextLine();
	        List<String> l = new ArrayList<String>();
	        Set<Character> se = new HashSet<Character>();
	        
	        for(int j=0;j<t;j++){
	        	s=in.nextLine();
	        	l.add(s);
	        	popSet(se,s);
	        }
	        
	        int tot = 0;
	        for(Character c:se){
	        	tot = getTot(l,se);
	        }
	        
	        System.out.println(tot);
	    }

		private static int getTot(List<String> l, Set<Character> se) {
			// TODO Auto-generated method stub
			int ret = 0;
			for(Character c:se){
				int tm = 0;
				for(String s:l){
					if (s.indexOf(c)>=0){
						tm++;
					}
				}
				if(tm==l.size()){
					ret++;
				}
			}
			
			return ret;
		}

		private static void popSet(Set<Character> se, String s) {
			// TODO Auto-generated method stub
			for(int i=0;i<s.length();i++){
				se.add(s.charAt(i));
			}
			
		}

	}
