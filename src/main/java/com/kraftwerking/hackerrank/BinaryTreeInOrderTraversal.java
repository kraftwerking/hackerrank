package com.kraftwerking.hackerrank;

import java.util.*;
import java.io.*;

class Nbode {
	int data;
	Nbode left;
	Nbode right;

	public Nbode(int data, Nbode left, Nbode right) {
		super();
		this.data = data;
		this.left = left;
		this.right = right;
	}

	public Nbode() {
		super();
		this.data = 0;
		this.left = null;
		this.right = null;
	}

}

class BinaryTreeInOrderTraversal {
	/*
	 * 
	 * class Nbode int data; Nbode left; Nbode right;
	 */
	/*
	 * Nbode is defined as : class Nbode int data; Nbode left; Nbode right;
	 * 
	 */

	void inOrder(Nbode root) {
		if (root != null) {
			printIn(root);
		}
	}

	void printIn(Nbode root) {
		if (root != null) {
			if (root.left != null) {
				printIn(root.left);
			}
			System.out.print(root.data + " ");
			if (root.right != null) {
				printIn(root.right);
			}
		}
	}

}
