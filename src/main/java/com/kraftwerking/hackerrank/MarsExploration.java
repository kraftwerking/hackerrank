package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class MarsExploration {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String S = in.next();
        
        int ret=0;
        for(int i=0;i<S.length();i=i+3){
        	//System.out.println(i);
        	String s = S.substring(i, i+3);
        	ret+=howMany(s);
        	//System.out.println(s);
        }
        System.out.println(ret);
    }

	private static int howMany(String s) {
		// TODO Auto-generated method stub
		int r = 0;
		if(s.charAt(0)!='S'){
			r++;
		} 
		if(s.charAt(1)!='O'){
			r++;
		}
		if(s.charAt(2)!='S'){
			r++;
		}
		return r;
	}
    
}
