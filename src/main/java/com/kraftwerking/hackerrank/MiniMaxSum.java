package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class MiniMaxSum {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        long a = in.nextLong();
        long b = in.nextLong();
        long c = in.nextLong();
        long d = in.nextLong();
        long e = in.nextLong();
                
        ArrayList<Long> arrayList = new ArrayList<Long>();
        arrayList.add(a);
        arrayList.add(b);
        arrayList.add(c);
        arrayList.add(d);
        arrayList.add(e);
        
        Long l = Collections.max(arrayList);
        //System.out.println(l);
        
        Long k = Collections.min(arrayList);
        //System.out.println(k);
        
        Long sum = arrayList.stream().mapToLong(Long::longValue).sum();
        //System.out.println(sum);
        System.out.print(sum-l);
        System.out.print(" ");
        System.out.print(sum-k);


    }
}
