package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class BalancedDelimiters {

	public static void main(String[] args) throws IOException {
		/*
		 * Enter your code here. Read input from STDIN. Print output to STDOUT.
		 * Your class should be named Solution.
		 */
		 Scanner scanner = new Scanner(System.in);
	        String str = scanner.nextLine();
	        char[] strArr = str.toCharArray();
	        ArrayList<Character> delims = new ArrayList<Character>();
	        String result = "True";
	        if(str.length()%2!=0){
	        	result = "False";
	        } else {
		        for(char i : strArr) {

		        	if (i == '('){
		        		delims.add(')');
		        	} else if (i == '['){
		        		delims.add(']');
		        	} else if (i == '{'){
		        		delims.add('}');
		        	} else {
		        		if(i == delims.get(delims.size()-1)){
		        			delims.remove(delims.size()-1);
		        		} else {
		        			result="False";
		        			break;
		        		}
		        	}
		        	
		        }
	        }

	        
	        System.out.print(result);
	}

}