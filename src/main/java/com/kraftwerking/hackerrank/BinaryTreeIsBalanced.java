package com.kraftwerking.hackerrank;


public class BinaryTreeIsBalanced {
	public boolean checkBalance(Nodely root){
		int result = isBalanced(root);
		if(result>0){
			return true;
		}else{
			return false;
		}
	}
	public int isBalanced(Nodely root){
		if(root==null){
			return 0;
		}
		int leftH = isBalanced(root.left);
		if(leftH==-1){
			return -1;
		}
		int rightH = isBalanced(root.right);
		if(rightH==-1){
			return -1;
		}
		int diff = leftH-rightH;
		if(Math.abs(diff)>1){
			return -1;
		}
		return 1 + Math.max(leftH, rightH);
	}
	public static void main(String args[]){
		Nodely root = new Nodely(5);
		root.left = new Nodely(10);
		root.right = new Nodely(15);
		root.left.left = new Nodely(20);
		root.left.right = new Nodely(25);
		root.right.left = new Nodely(30);
		root.right.right = new Nodely(35);		
		System.out.println(" Is Tree Balanced : " + (new BinaryTreeIsBalanced()).checkBalance(root));
		root.right.right.right = new Nodely (40);
		root.right.right.right.right = new Nodely (45);
		System.out.println(" Is Tree Balanced : " + (new BinaryTreeIsBalanced()).checkBalance(root));
	}
}
class Nodely{
	int data;
	Nodely left;
	Nodely right;
	public Nodely(int data){
		this.data = data;
		this.left = null;
		this.right =null;
	}
}