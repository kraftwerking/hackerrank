package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class TimeConversion {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String[] str = in.nextLine().split(":");
		
		int hr=Integer.parseInt(str[0]);
		String m=str[1];
		String sstr=str[2];		
		String ampm = sstr.length() > 2 ? sstr.substring(sstr.length() - 2) : sstr;
		String s= sstr.substring(0, 2);
		
		if(ampm.equals("PM")){
			if(hr!=12){
				hr+=12;				
			}

		} else if (ampm.equals("AM") && hr==12){
			hr=00;
		}
		
		String h=Integer.toString(hr);
		if(h.length()==1){
			h="0"+h;
		} else if(m.length()==1){
			m="0"+m;
		} else if(s.length()==1){
			s="0"+s;
		}

		System.out.println(h+":"+m+":"+s);


	}
	
}
