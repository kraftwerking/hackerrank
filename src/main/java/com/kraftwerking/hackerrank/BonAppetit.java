package com.kraftwerking.hackerrank;

import java.util.Scanner;

public class BonAppetit {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int k = sc.nextInt();
        int real = 0;
        for(int i=0;i<n;i++){
            int tm = sc.nextInt();
            if(i!=k){
                real = real + tm;
            }
        }
        int b = sc.nextInt();
        int realhalf = real/2;
        if(b==realhalf){
            System.out.println("Bon Appetit");
        } else {
            int tm = b-realhalf;
            System.out.println(tm);
        }

    }
}