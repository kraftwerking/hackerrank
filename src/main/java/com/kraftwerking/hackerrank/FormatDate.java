package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class FormatDate {

	/*
	 * Complete the function below.
	 */

	    static String[] reformatDate(String[] dates) {
	    	String[] res = new String[dates.length];
	    	String tmp = "";
	    	int count=0;
	    	for(String s:dates){
	    		String[] spl = s.split("\\s+");
	    		String d = spl[0];
	    		String m = spl[1];
	    		String y = spl[2];
	    		
	    		d=d.substring(0, d.length() - 2);
	    		if(d.length()==1){
	    			d="0"+d;
	    		}
	    		
	    		if(m.equals("Jan")){
	    			m="01";
	    		} else if(m.equals("Feb")){
	    			m="02";
	    		} else if(m.equals("Mar")){
	    			m="03";
	    		} else if(m.equals("Apr")){
	    			m="04";
	    		} else if(m.equals("May")){
	    			m="05";
	    		} else if(m.equals("Jun")){
	    			m="06";
	    		} else if(m.equals("Jul")){
	    			m="07";
	    		} else if(m.equals("Aug")){
	    			m="08";
	    		} else if(m.equals("Sep")){
	    			m="09";
	    		} else if(m.equals("Oct")){
	    			m="10";
	    		} else if(m.equals("Nov")){
	    			m="11";
	    		} else if(m.equals("Dec")){
	    			m="12";
	    		}

	    		tmp = y + "-" + m + "-" + d;
	    		
	    		res[count]=tmp;
	    		count++;
	    	}
	    	return res;
	    }



    public static void main(String[] args) throws IOException{
        Scanner in = new Scanner(System.in);
        final String fileName = System.getenv("OUTPUT_PATH");
        BufferedWriter bw = new BufferedWriter(new FileWriter(fileName));
        String[] res;
        
        int _dates_size = 0;
        _dates_size = Integer.parseInt(in.nextLine().trim());
        String[] _dates = new String[_dates_size];
        String _dates_item;
        for(int _dates_i = 0; _dates_i < _dates_size; _dates_i++) {
            try {
                _dates_item = in.nextLine();
            } catch (Exception e) {
                _dates_item = null;
            }
            _dates[_dates_i] = _dates_item;
        }
        
        res = reformatDate(_dates);
        for(int res_i=0; res_i < res.length; res_i++) {
            bw.write(String.valueOf(res[res_i]));
            bw.newLine();
        }
        
        bw.close();
    }
}