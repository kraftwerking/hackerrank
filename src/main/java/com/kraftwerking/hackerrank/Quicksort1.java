package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;

public class Quicksort1 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int ar[] = new int[n];
        for(int i=0; i < n; i++){
            ar[i] = in.nextInt();
        }
        
        partitionSort(n,ar);
        
    }

	private static void partitionSort(int n, int[] ar) {
		// TODO Auto-generated method stub
		
		//choose a random num but set this first
		int piv=ar[0]; //pivot of 4
		
		List<Integer> l = new ArrayList<Integer>(); 
		List<Integer> r = new ArrayList<Integer>(); 
		
		for(int a:ar){
			if(a>=piv){
				r.add(a);
			} else if (a<piv){
				l.add(a);
			}
		}
		
		for(int i:l){
	        System.out.print(i + " ");
		}
		for(int i:r){
	        System.out.print(i + " ");
		}

	}
    

}
