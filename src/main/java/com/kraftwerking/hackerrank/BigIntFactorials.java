package com.kraftwerking.hackerrank;

import java.math.BigInteger;
import java.util.Scanner;

public class BigIntFactorials {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();

        BigInteger bigInt = fibSeries( BigInteger.valueOf(n));
        System.out.print(bigInt);


    }

    private static  BigInteger fibSeries(BigInteger n){
        BigInteger big1 =new BigInteger("1");

        BigInteger fibNumber =big1;
        if(n.intValue() == 0)
            return big1;

        return n.multiply(fibSeries(n.subtract(big1)));



    }
}