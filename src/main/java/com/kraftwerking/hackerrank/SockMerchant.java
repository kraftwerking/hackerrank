package com.kraftwerking.hackerrank;

import java.util.Arrays;
import java.util.Scanner;

public class SockMerchant {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int c[] = new int[n];
        for(int c_i=0; c_i < n; c_i++){
            c[c_i] = in.nextInt();
        }
        int cn = uniquePairs(c);
        System.out.println(cn);
    }

    public static int uniquePairs(int[] a) {
        Arrays.sort(a);
        // now we have: [1, 1, 1, 4, 4, 5, 5, 7, 7]

        int res = 0;
        int len = a.length;
        int i = 0;

        while (i < len) {
            // take first number
            int num = a[i];
            int c = 1;
            i++;

            // count all duplicates
            while(i < len && a[i] == num) {
                c++;
                i++;
            }
            //System.out.println("Number: " + num + "\tCount: "+c);
            // if we spotted number just 2 times, increment result
            /*if (c == 2) {
                res++;
            }*/
            if(c>1){
                //System.out.println(c/2);
                int tm = c/2;
                res = res+tm;
            }


        }

        return res;
    }
}
