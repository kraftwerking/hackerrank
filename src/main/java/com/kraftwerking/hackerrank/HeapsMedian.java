package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class HeapsMedian {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] a = new int[n];
		for (int a_i = 0; a_i < n; a_i++) {
			a[a_i] = in.nextInt();
		}
		getMedians(a);

	}

	private static void getMedians(int[] a) {
		// TODO Auto-generated method stub
		List<Integer> nw = new ArrayList<Integer>();

		for (int i = 0; i < a.length; i++) {
			nw.add(a[i]);
			Collections.sort(nw);
			int val = 0;
			int val2=0;
			if (i == 0) {
				System.out.println((double) nw.get(0));
			} else {
				if (nw.size() % 2 == 1) {
					// System.out.println(nw.toString());
					// System.out.println(Math.floor(nw.size()/2.0));
					val = (int) Math.floor(nw.size() / 2.0);
					System.out.println(((double)nw.get(val)));
				} else {
					val=nw.get(nw.size()/2);
					val2=nw.get((nw.size()-1)/2);
					//System.out.println(val);
					//System.out.println(val2);
					System.out.println((val+val2) / 2.0);

				}
			}
		}

	}

}
