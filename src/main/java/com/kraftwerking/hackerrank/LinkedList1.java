package com.kraftwerking.hackerrank;

import java.util.*;

public class LinkedList1 {
	/*
	 * Detect a cycle in a linked list. Note that the head pointer may be 'null'
	 * if the list is empty.
	 * 
	 * A Node is defined as: class Node { int data; Node next; }
	 */
	class Node {
		int data;
		Node next;
	}

	boolean hasCycle(Node head) {
	    Set<Node> seen = new HashSet<>();
	    while (head != null) {
	        seen.add(head);
	        head = head.next;
	        if (seen.contains(head)) return true;
	    }
	    return false;
	}
	
}
