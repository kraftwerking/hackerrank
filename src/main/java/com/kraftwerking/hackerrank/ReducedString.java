package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class ReducedString {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String s = in.nextLine();
		ArrayList<Character> c = new ArrayList<Character>();
		for (char ch : s.toCharArray()) {
			c.add(ch);
		}

		Collections.sort(c); // Sort a
		int i = 0;
		while (i < c.size() - 1) {
			// Check if i and i+1 are the same element. If so, remove both
			if (c.get(i).equals(c.get(i + 1))) {
				// Remove i twice - effectively removes i and i+1
				c.remove(i);
				c.remove(i);

				// Move i *back* one index, which is equivalent to
				// moving forward one because we just removed two elements.
				// Prevent i from becoming negative though.
				i = Math.max(0, (i - 1));
			} else {
				i++;
			}
		}
		if (c.isEmpty()) {
			System.out.println("Empty String");
		} else {
			for (Character temp : c) {
				System.out.print(temp);
			}
		}
	}

}
