package com.kraftwerking.hackerrank;

import java.util.Scanner;

public class UtopianTree {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        int res = 0;

        for(int i=0;i<t;i++){
            int n = sc.nextInt();
            getHt(n);
        }

    }

    private static void getHt(int n) {
        int res = 1;
        if(n==0){
            System.out.println(res);
        } else {
            for(int i=1;i<=n;i++){
                if(i%2!=0){
                    res = res*2;
                } else {
                    res = res+1;
                }
            }
            System.out.println(res);
        }

    }
}