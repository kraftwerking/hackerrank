package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;


class Difference {

	private int[] elements;
  	public int maximumDifference;
// Add your code here
  	public Difference(int[] a) {
		// TODO Auto-generated constructor stub
  		this.elements = a;
	}
  	
  	
	public void computeDifference() {
		// TODO Auto-generated method stub
		int max = elements[0];
		int min = elements[0];
		for(int i=0;i<elements.length;i++){
			if(elements[i]>max){
				max=elements[i];
			}
			if(elements[i]<min){
				min=elements[i];
			}
			
		}
		maximumDifference=Math.abs(max-min);
		
	}



} // End of Difference class

public class Scope {

    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        int[] a = new int[n];
        for (int i = 0; i < n; i++) {
            a[i] = sc.nextInt();
        }
        sc.close();

        Difference difference = new Difference(a);

        difference.computeDifference();

        System.out.print(difference.maximumDifference);
    }
}