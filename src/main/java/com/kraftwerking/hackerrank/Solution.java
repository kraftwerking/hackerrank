package com.kraftwerking.hackerrank;
import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class Solution {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int k = in.nextInt();
        int[] height = new int[n];
        for(int height_i=0; height_i < n; height_i++){
            height[height_i] = in.nextInt();
        }
        // your code goes here
        int max = Arrays.stream(height).max().getAsInt();
        //System.out.println(max);

        if(k>=max){
            System.out.println(0);
        } else {
            int diff = max-k;
            System.out.println(diff);

        }
    }
}
