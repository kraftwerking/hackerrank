package com.kraftwerking.hackerrank;

import java.util.*;
import java.io.*;

class Nodd {
	int data;
	Nodd left;
	Nodd right;
	public Nodd(int data, Nodd left, Nodd right) {
		super();
		this.data = data;
		this.left = left;
		this.right = right;
	}
	public Nodd() {
		super();
		this.data = 0;
		this.left = null;
		this.right = null;
	}
	
}

class BinaryTreeInsert {
	/*
	 * 
	 * class Nodd int data; Nodd left; Nodd right;
	 */
	 /* Nodd is defined as :
	 class Nodd 
	    int data;
	    Nodd left;
	    Nodd right;
	    
	    */

		static Nodd Insert(Nodd root, int value) {
			if (root != null) {
				Nodd news = new Nodd();
	            news.data = value;
				findInsertionPoint(root, news);
			} else {
	            Nodd node=new Nodd();
	             node.data=value;
	             node.left=null;
	             node.right=null;
	             root=node;
	        }
			return root;
		}

		private static void findInsertionPoint(Nodd root, Nodd news) {
			// TODO Auto-generated method stub
			// System.out.println("yo");
			if (root != null) {
				if (news.data < root.data && root.left != null) {
					findInsertionPoint(root.left, news);
				} else if (news.data < root.data && root.left == null) {
	                root.left=news;
				}

				if (news.data > root.data && root.right != null) {
					findInsertionPoint(root.right, news);
				} else if (news.data > root.data && root.right == null) {
	                root.right=news;
				}
			}

		}

}
