package com.kraftwerking.hackerrank;

import java.util.*;
import java.io.*;

class Nodelyy {
	Nodelyy left, right;
	int data;

	Nodelyy(int data) {
		this.data = data;
		left = right = null;
	}
}

class BinaryTreeHeight {

	/*
	 * class Nodelyy int data; Nodelyy left; Nodelyy right;
	 */

	static int getHeight(Nodelyy root) {
		if (root == null)
			return -1;
		else
			return 1 + java.lang.Math.max(getHeight(root.left), getHeight(root.right));
	}

	public static Nodelyy insert(Nodelyy root, int data) {
		if (root == null) {
			return new Nodelyy(data);
		} else {
			Nodelyy cur;
			if (data <= root.data) {
				cur = insert(root.left, data);
				root.left = cur;
			} else {
				cur = insert(root.right, data);
				root.right = cur;
			}
			return root;
		}
	}

	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		int T = sc.nextInt();
		Nodelyy root = null;
		while (T-- > 0) {
			int data = sc.nextInt();
			root = insert(root, data);
		}
		int height = getHeight(root);
		System.out.println(height);
	}
}