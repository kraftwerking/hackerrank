package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;
import com.kraftwerking.hackerrank.CtCILibrary.*;

public class FindLinkedListNode {

	public static void main(String[] args) {	
		LinkedListNode first = new LinkedListNode(0, null, null); //AssortedMethods.randomLinkedList(1000, 0, 2);
		LinkedListNode head = first;
		LinkedListNode second = first;
		for (int i = 1; i < 8; i++) {
			second = new LinkedListNode(i % 2, null, null);
			first.setNext(second);
			second.setPrevious(first);
			first = second;
		}
		int f=3;
		findNth(head,f);
		
	}

	
	private static void findNth(LinkedListNode cc, int f) {
		// TODO Auto-generated method stub
		LinkedListNode n=cc;
		int c = 0;
		while (n != null) {
			c++;
			n = n.next;
		}
		//System.out.println(c);
		int stop = c-f;
		//System.out.println(stop);
		c=0;
		n=cc;
		while (n != null) {
			if(c==stop){
				System.out.println(n.data);
			}
			c++;
			n = n.next;
		}
	}


	private static void deleteDupsA(LinkedListNode n) {
		HashSet<Integer> set = new HashSet<Integer>();
		LinkedListNode previous = null;
		while (n != null) {
			if (set.contains(n.data)) {
				previous.next = n.next;
			} else {
				set.add(n.data);
				previous = n;
			}
			n = n.next;
		}
	}
}