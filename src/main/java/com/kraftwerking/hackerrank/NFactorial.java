package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class NFactorial {

	public static void main(String[] args) throws IOException {
		/*
		 * Enter your code here. Read input from STDIN. Print output to STDOUT.
		 * Your class should be named Solution.
		 */
		Scanner scanner = new Scanner(System.in);
		String str = scanner.nextLine();
		int val = Integer.parseInt(str);
		int res = 0;
		if (val == 0) {
			res = 1;
		} else {
			for (int i = val; i > 0; i--) {
				// System.out.println(i);
				if (i == val) {
					res = val;
				} else {
					res = res * i;
				}
			}
		}

		System.out.println(res);

	}

}