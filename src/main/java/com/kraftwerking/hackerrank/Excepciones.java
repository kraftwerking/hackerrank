package com.kraftwerking.hackerrank;

import java.util.*;
import java.io.*;

class Calculatorr{

	public int power(int n, int p) throws Exception{
		// TODO Auto-generated method stub
		if(n<0 || p<0){
			throw new Exception("n and p should be non-negative");
		}
	     return (int) Math.pow(n,p);
	}
	
	
}

class Excepciones{

    public static void main(String []argh)
    {
        Scanner in = new Scanner(System.in);
        int T=in.nextInt();
        while(T-->0)
        {
            int n = in.nextInt();
            int p = in.nextInt();
            Calculatorr myCalculatorr = new Calculatorr();
            try
            {
                int ans=myCalculatorr.power(n,p);
                System.out.println(ans);
                
            }
            catch(Exception e)
            {
                System.out.println(e.getMessage());
            }
        }

    }
}
