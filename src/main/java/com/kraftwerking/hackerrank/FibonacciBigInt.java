package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class FibonacciBigInt {

	public static void main(String[] args) throws IOException {
		/*
		 * Enter your code here. Read input from STDIN. Print output to STDOUT.
		 * Your class should be named Solution.
		 */
		BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
		String line;

		while ((line = stdin.readLine()) != null) {
			BigInteger bigInt = new BigInteger(line);
			System.out.println(fibonacci(bigInt));
		}

	}

	private static BigInteger fibonacci(BigInteger bigInt) {
		// TODO Auto-generated method stub

		if (bigInt.equals(BigInteger.ZERO)) {
			return BigInteger.ZERO;
		} else if (bigInt.equals(BigInteger.ONE)) {
			return BigInteger.ONE;
		} else if (bigInt.equals(2)) {
			return BigInteger.ONE;
		} else {
			BigInteger x;
			BigInteger y;
			x = fibonacci(bigInt.subtract(BigInteger.valueOf(1)));
            y = fibonacci(bigInt.subtract(BigInteger.valueOf(2)));
            
            return  x.add(y) ;		}

	}

}