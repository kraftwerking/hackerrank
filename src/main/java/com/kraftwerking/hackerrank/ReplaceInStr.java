package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class ReplaceInStr {

	public static void main(String[] args) {
		StringBuffer sb = new StringBuffer();
		String s = "now is the time for all good men to come to the aid";
		for(int i=0;i<s.length();i++){
			if(s.charAt(i)==' '){
				sb.append("%20");
			} else{
				sb.append(s.charAt(i));
			}
		}
		System.out.println(sb.toString());
	}

}