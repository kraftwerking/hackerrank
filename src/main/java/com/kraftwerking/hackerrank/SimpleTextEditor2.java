package com.kraftwerking.hackerrank;

import java.util.Arrays;
import java.util.List;
import java.util.Scanner;
import java.util.Stack;

public class SimpleTextEditor2 {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        int n = sc.nextInt();
        String s = "";
        Stack<String> stack = new Stack();
        for(int i = 0; i<n;i++){
            int q = sc.nextInt();
            switch(q){
                case 1:
                    stack.add(s);
                    s += sc.next();
                    break;
                case 2: 
                    stack.add(s);
                    int k = sc.nextInt();
                    s = s.substring(0, s.length()-k);
                    break;
                case 3: 
                                System.out.println(s.charAt(sc.nextInt()-1));
                    break;
                case 4: 
                    s = stack.pop();
                    break;
            }
            
        }
    }
}