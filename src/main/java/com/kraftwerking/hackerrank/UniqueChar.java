package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class UniqueChar {

	public static void main(String[] args) {
		String[] words = {"abcde", "hello", "apple", "kite", "padle"};
		for (String word : words) {
			System.out.println(word + ": " + isUniqueChars(word));
		}
	}

	private static boolean isUniqueChars(String word) {
		// TODO Auto-generated method stub
		boolean ret=true;
		ArrayList<Character>c = new ArrayList<Character>();
		
		for(int i=0;i<=word.length()-1;i++){
			if(c.contains(word.charAt(i))){
				ret=false;
			} else{
				c.add(word.charAt(i));
			}
		}
		
		
		return ret;
	}

}