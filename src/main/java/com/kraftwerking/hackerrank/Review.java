package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;

public class Review	 {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        for(int j=0;j<n;j++){
            char[] a=in.next().toCharArray();
        	StringBuilder e = new StringBuilder();
        	StringBuilder o = new StringBuilder();
            for (int i=0;i<a.length;i++)
                if(i%2==0){
                	e.append(a[i]);
                } else {
                	o.append(a[i]);
                }
            System.out.println(e + " " + o);
            }
        }

}