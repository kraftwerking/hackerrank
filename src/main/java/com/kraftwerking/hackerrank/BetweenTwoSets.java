package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class BetweenTwoSets {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int m = in.nextInt();
		int[] a = new int[n];
		for (int a_i = 0; a_i < n; a_i++) {
			a[a_i] = in.nextInt();
		}
		int[] b = new int[m];
		for (int b_i = 0; b_i < m; b_i++) {
			b[b_i] = in.nextInt();
		}

		List<Integer> l = new ArrayList<Integer>();
		boolean ad = true;
		for (int i = a[n - 1]; i <= b[0]; i = i + a[n - 1]) {
			if (checkFac(i,a,b)){
				//System.out.println(i);	
				l.add(i);
			}

			}


		System.out.println(l.size());
	}

	private static boolean checkFac(int i, int[] a, int[] b) {
		// TODO Auto-generated method stub
		boolean ret = true;
		for(int in:a){
			if(i%in!=0){
				ret=false;
			}
		}
		for(int in:b){
			if(in%i!=0){
				ret=false;
			}
		}
		
		return ret;
	}

}
