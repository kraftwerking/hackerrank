package com.kraftwerking.hackerrank;
import com.kraftwerking.hackerrank.CtCILibrary.*;

//Complete this code or write your own from scratch
import java.util.*;
import java.io.*;

class LinkedListReverseAddition{
	public static void main(String[] args) {
		/* Create linked list */
		int[] vals = {3,1,5};
		LinkedListNode head = new LinkedListNode(vals[0], null, null);
		LinkedListNode current = head;
		for (int i = 1; i < vals.length; i++) {
			current = new LinkedListNode(vals[i], null, current);
		}
		System.out.println(head.printForward());
		
		int s1 = getTotal(current);
		
		int[] valss = {5,9,2};
		head = new LinkedListNode(valss[0], null, null);
		current = head;
		for (int i = 1; i < valss.length; i++) {
			current = new LinkedListNode(valss[i], null, current);
		}
		System.out.println(head.printForward());
		
		int s2 = getTotal(current);
		
		System.out.println(s1+s2);
	}

	private static int getTotal(LinkedListNode current) {
		// TODO Auto-generated method stub
		int ret =0;
		StringBuilder sb = new StringBuilder();
		while(current.prev!=null){
			sb.append(current.data);
			if(current.prev==null){
				sb.append(current.data);
			}
			current=current.prev;
		}
		sb.append(current.data);
		
		return Integer.parseInt(sb.toString());
	}
}
