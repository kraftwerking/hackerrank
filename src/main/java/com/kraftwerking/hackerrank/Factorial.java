	package com.kraftwerking.hackerrank;
	
	import java.io.*;
	import java.util.*;
	import java.text.*;
	import java.math.*;
	import java.util.regex.*;
	
	public class Factorial {
		
		public static int factorial(int i){
			int ret = 0;
			if (i==1){
				ret=1;
			} else {
				ret = i*factorial(i-1);			
			}
		
			return ret;
		}
	
	    public static void main(String[] args) {
	        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
			Scanner sc = new Scanner(System.in);
			int n = sc.nextInt();
			System.out.print(factorial(n));
	    }
	}