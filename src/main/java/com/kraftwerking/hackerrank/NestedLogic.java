package com.kraftwerking.hackerrank;


import java.util.Scanner;

public class NestedLogic {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        int d = sc.nextInt();
        int m = sc.nextInt();
        int y = sc.nextInt();

        int exd = sc.nextInt();
        int exm = sc.nextInt();
        int exy = sc.nextInt();

        int ret = 0;

        if (y > exy) { //more th a year late
            ret = 10000;

        } else if (y == exy) { //more th a month late
            if (m > exm) { //more th a month late
                int numo = m - exm;
                ret = 500 * numo;

            } else if (d > exd) { //more th x days late
                int numd = d - exd;
                ret = 15 * numd;
            }
        }

        System.out.println(ret);
    }
}
