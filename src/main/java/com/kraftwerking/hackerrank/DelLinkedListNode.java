package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;
import com.kraftwerking.hackerrank.CtCILibrary.*;

public class DelLinkedListNode {


	
	public static void main(String[] args) {
		LinkedListNode head = AssortedMethods.randomLinkedList(10, 0, 10);
		System.out.println(head.printForward());
		deleteNode(head.next.next.next.next); // delete node 4
		//System.out.println(head.printForward());
	}

	private static void deleteNode(LinkedListNode next) {
		// TODO Auto-generated method stub
		int del = next.data;
		while(next.prev != null){
			//System.out.println(next.printForward());
			next=next.prev;
			if(next.prev==null){
				break;
			}
		}
		System.out.println(next.printForward());
		while(next.next!=null){

			if(next.next.data==del){
				next.next=next.next.next;
				break;
			}
			next=next.next;
		}
		System.out.println(next.printForward());
	}

}