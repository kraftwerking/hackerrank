package com.kraftwerking.hackerrank;

import java.util.*;
import java.io.*;

class Ndedeo {
	Ndedeo left, right;
	int data;

	Ndedeo(int data) {
		this.data = data;
		left = right = null;
	}
}

class BinaryTreeLowestCommonAncestor {

	/*
	 * Ndedeo is defined as : class Ndedeo int data; Ndedeo left; Ndedeo right;
	 * 
	 */

	static Ndedeo lca(Ndedeo root, int v1, int v2) {
		if (root == null) {
			return root;
		}
		Stack<Ndedeo> s = new Stack<Ndedeo>();
		Ndedeo th1 = findIt(root, v1, s);
		Stack<Ndedeo> s2 = new Stack<Ndedeo>();
		Ndedeo th2 = findIt(root, v2, s2);
		
/*        for(int i=0;i<s.size();i++){
        	System.out.println(s.pop().data);
        }
        System.out.println("");
        for(int i=0;i<s.size();i++){
        	System.out.println(s2.pop().data);
        }*/
		int tm =0;
		Ndedeo x = null;
		while(!s.isEmpty()){
			x = s.pop();
			System.out.println(x.data);
			if(s2.contains(x)){
				return x;
			}
		}
		return x;
	}

	private static Ndedeo findIt(Ndedeo root, int v1, Stack<Ndedeo> s) {
		// TODO Auto-generated method stub
		if(root!=null){
			if(v1<root.data){
				s.push(root);
				findIt(root.left,v1,s);
			} else if(v1>root.data){
				s.push(root);
				findIt(root.right,v1,s);
			} else if(v1==root.data){
				//System.out.println("found " + root.data);
			}
		}
		
		return null;
	}

}