package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class TimeComplexityPrimality {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int p = in.nextInt();
        for(int a0 = 0; a0 < p; a0++){
            int n = in.nextInt();
            testForPrime(n);
        }
    }

	private static void testForPrime(int n) {
		// TODO Auto-generated method stub
		boolean pr = false;

		int l = (int) Math.sqrt(n);
		for(int i=2;i<=l;i++){
			if(n%i==0){
				pr=true;
				break;
			}
		}
		if(n==1){
			System.out.println("Not prime");
		} else if(pr){		
			System.out.println("Not prime");
		} else {
			System.out.println("Prime");

		}
		
	}
}
