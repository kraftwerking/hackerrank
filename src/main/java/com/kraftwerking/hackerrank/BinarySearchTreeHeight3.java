package com.kraftwerking.hackerrank;

import java.util.Scanner;

class Noonononode{
    Noonononode left,right;
    int data;
    Noonononode(int data){
        this.data=data;
        left=right=null;
    }
}

class BinarySearchTreeHeight3{
    
    
	static int getHeight(Noonononode root) {
		// Write your code here.
		return getHt(root);
	}
	
	static int getHt(Noonononode root){

		if(root==null){
			return -1;
		} else {
			int lht = getHt(root.left);
			int rht = getHt(root.right);
			if(lht>rht){
				return lht+1;
			} else {
				return rht+1;
			}
			
		}

	}
	
	public static Noonononode insert(Noonononode root,int data){
        if(root==null){
            return new Noonononode(data);
        }
        else{
            Noonononode cur;
            if(data<=root.data){
                cur=insert(root.left,data);
                root.left=cur;
            }
            else{
                cur=insert(root.right,data);
                root.right=cur;
            }
            return root;
        }
    }
    public static void main(String args[]){
            Scanner sc=new Scanner(System.in);
            int T=sc.nextInt();
            Noonononode root=null;
            while(T-->0){
                int data=sc.nextInt();
                root=insert(root,data);
            }
            int height=getHeight(root);
            System.out.println(height);
        }	
}