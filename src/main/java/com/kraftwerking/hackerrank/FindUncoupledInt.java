package com.kraftwerking.hackerrank;


import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class FindUncoupledInt {

	public static void main(String[] args) throws IOException {
		/*
		 * Enter your code here. Read input from STDIN. Print output to STDOUT.
		 * Your class should be named Solution.
		 */
		 Scanner scanner = new Scanner(System.in);
	        String[] str = scanner.nextLine().split(",");
	        int result = 0;
	        //Computes the bitwise-XOR, ^, of all the elements of an array,
	   	    //to find the unpaired element in O(n) time.
	        for(String i : str) 
	            result ^= Integer.parseInt(i.trim());
	        System.out.println(result);
	}

}