package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class EqualStacks {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n1 = in.nextInt();
        int n2 = in.nextInt();
        int n3 = in.nextInt();
        int h1[] = new int[n1];
        for(int h1_i=0; h1_i < n1; h1_i++){
            h1[h1_i] = in.nextInt();
        }
        int h2[] = new int[n2];
        for(int h2_i=0; h2_i < n2; h2_i++){
            h2[h2_i] = in.nextInt();
        }
        int h3[] = new int[n3];
        for(int h3_i=0; h3_i < n3; h3_i++){
            h3[h3_i] = in.nextInt();
        }
        
        Stack<StackNode>s1=new Stack<StackNode>();
        Stack<StackNode>s2=new Stack<StackNode>();
        Stack<StackNode>s3=new Stack<StackNode>();
        int h1Sum = 0;
        int h2Sum = 0;
        int h3Sum = 0;

        for(int i=h1.length-1;i>=0;i--){
        	h1Sum+=h1[i];
        	StackNode tm = new StackNode(h1[i],h1Sum);
        	s1.push(tm);
        }
        for(int i=h2.length-1;i>=0;i--){
        	h2Sum+=h2[i];
        	StackNode tm = new StackNode(h2[i],h2Sum);
        	s2.push(tm);
        }
        for(int i=h3.length-1;i>=0;i--){
        	h3Sum+=h3[i];
        	StackNode tm = new StackNode(h3[i],h3Sum);
        	s3.push(tm);
        }
        
        List<Integer>ss = new ArrayList<Integer>();
        List<Integer>ss2 = new ArrayList<Integer>();
        List<Integer>ss3 = new ArrayList<Integer>();
        
        while(!s1.isEmpty()){
        	ss.add(s1.peek().max);
        	s1.pop();
        }
        while(!s2.isEmpty()){
        	ss2.add(s2.peek().max);
        	s2.pop();
        }
        while(!s3.isEmpty()){
        	ss3.add(s3.peek().max);
        	s3.pop();
        }

        ss.retainAll(ss2);
        ss.retainAll(ss3);
        
        System.out.println(Collections.max(ss));
    }
	



	static class StackNode {
		int val;
		int max;

		public StackNode(int val, int max) {
			super();
			this.val = val;
			this.max = max;
		}

		@Override
		public String toString() {
			return "StackNode [val=" + val + ", max=" + max + "]";
		}

	}
}
