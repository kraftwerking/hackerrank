package com.kraftwerking.hackerrank;

import java.util.Scanner;

public class ViralAdvertising {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();
        double ret = 0;
        double x = 5;
        double f =0;
        for(int i=1;i<=t;i++){
            double tm = Math.floor(x/2);
            //System.out.println(tm);
            ret = ret + tm;
            x = tm *3;
        }

        sc.close();
        System.out.println((int)ret);

    }


}