package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class CompareTriplets {

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int a0 = in.nextInt();
        int a1 = in.nextInt();
        int a2 = in.nextInt();
        int b0 = in.nextInt();
        int b1 = in.nextInt();
        int b2 = in.nextInt();
        int[] a = new int[3];
        int[] b = new int[3];
        a[0]=a0;
        a[1]=a1;
        a[2]=a2;
        b[0]=b0;
        b[1]=b1;
        b[2]=b2;
        
        int bob=0;
        int alice=0;
        for(int i=0;i<a.length;i++){
        	if(a[i]>b[i]){
        		alice++;
        	} else if(a[i]<b[i]){
        		bob++;
        	}  
        }
        
        System.out.println(alice + " " + bob);
    }
}
