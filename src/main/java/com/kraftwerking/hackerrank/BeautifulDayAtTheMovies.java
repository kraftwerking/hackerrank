package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class BeautifulDayAtTheMovies {

	private static long i;
	private static long j;
	private static long k;
	
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        i = in.nextLong();
        j = in.nextLong();
        k = in.nextLong();
        int ret = 0;
                
        ArrayList<Long> arr = new ArrayList<Long>();
        for(long x=i;x<=j;x++){
        	arr.add(x);
        }
        
        for (Long lng : arr) {
			if(isBeautiful(lng)){
				ret++;
			}
		}
        
        System.out.println(ret);
    }
    
    private static boolean isBeautiful(Long lng) {
		// TODO Auto-generated method stub
    	long dng = lng - (reverse(lng));
    	if(dng%k==0){
    		return true;
    	} else {
    		return false;

    	}
	}

	public static long reverse(long l){
    	
    	char[] chars = ("" + l).toCharArray();
    	//System.out.println(new String(chars));

    	char[] reversed = new StringBuilder(new String(chars)).reverse().toString().toCharArray();
    	
    	return Long.parseLong(new String(reversed));
    }
}
