package com.kraftwerking.hackerrank;

import java.util.Scanner;

public class SaveThePrisoner {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();

        for(int i=0;i<t;i++){
            int n = sc.nextInt();
            int m = sc.nextInt();
            int s = sc.nextInt();

            long poisoned = (s + m - 1) % n;
            if(poisoned == 0)
                poisoned = n;
            System.out.println(poisoned);
        }

        sc.close();

    }


}