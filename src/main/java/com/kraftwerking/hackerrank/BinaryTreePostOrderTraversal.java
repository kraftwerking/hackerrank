package com.kraftwerking.hackerrank;

import java.util.*;
import java.io.*;

class Noxxd {
	int data;
	Noxxd left;
	Noxxd right;

	public Noxxd(int data, Noxxd left, Noxxd right) {
		super();
		this.data = data;
		this.left = left;
		this.right = right;
	}

	public Noxxd() {
		super();
		this.data = 0;
		this.left = null;
		this.right = null;
	}

}

class BinaryTreePostOrderTraversal {
	/*
	 * 
	 * class Noxxd int data; Noxxd left; Noxxd right;
	 */
	/*
	 * Noxxd is defined as : class Noxxd int data; Noxxd left; Noxxd right;
	 * 
	 */

	void postOrder(Noxxd root) {
		if (root != null) {
			printPost(root);
		}
	}

	void printPost(Noxxd root) {
		if (root != null) {
			if (root.left != null) {
				printPost(root.left);
			}
			if (root.right != null) {
				printPost(root.right);
			}
			System.out.print(root.data + " ");

		}
	}

}
