package com.kraftwerking.hackerrank;

import com.kraftwerking.hackerrank.CtCILibrary.*;

//Complete this code or write your own from scratch
import java.util.*;
import java.io.*;

class StackMaxVal {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		int max = Integer.MIN_VALUE;
		Stack<StackNode> stack = new Stack<StackNode>();

		while (n > 0) {
			int choice = sc.nextInt();
			if (choice == 1) {
				int val = sc.nextInt();
				max = Math.max(val, max);

				stack.push(new StackNode(val, max));
			} else if (choice == 2) {
				if (!stack.isEmpty()){
					stack.pop();
				} //reset max
				if (stack.isEmpty()) {
					max=Integer.MIN_VALUE;
				} else {
					max=stack.peek().curMax;
				}

			} else if (choice == 3) {
				int x = stack.peek().curMax;
				System.out.println(x);
			}

			n--;
		}
		sc.close();
	}

	static class StackNode {
		int val;
		int curMax;

		public StackNode(int val, int curMax) {
			super();
			this.val = val;
			this.curMax = curMax;
		}

		@Override
		public String toString() {
			return "StackNode [val=" + val + ", curMax=" + curMax + "]";
		}

	}

}
