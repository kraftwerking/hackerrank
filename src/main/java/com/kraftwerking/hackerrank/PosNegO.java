package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class PosNegO {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		in.nextLine();
		int pos=0;
		int neg=0;
		int o=0;
		
		String[] str = in.nextLine().split(" ");
		for(String s:str){
			//System.out.println(Integer.parseInt(s));
			if(Integer.parseInt(s)>0){
				pos++;
			} else if(Integer.parseInt(s)<0){
				neg++;
			} else {
				o++;
			}
		}
		in.close();
        DecimalFormat df = new DecimalFormat("#.######");
        System.out.println(df.format((double)pos/n));
        System.out.println(df.format((double)neg/n));
        System.out.println(df.format((double)o/n));



	}

}
