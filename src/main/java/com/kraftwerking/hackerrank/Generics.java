package com.kraftwerking.hackerrank;

import java.lang.reflect.Method;

class Generics {

	// Write your code here

	public static void main(String args[]) {
		Integer[] intArray = { 1, 2, 3 };
		String[] stringArray = { "Hello", "World" };

		printArray(intArray);
		printArray(stringArray);

		if (Generics.class.getDeclaredMethods().length > 2) {
			System.out.println("You should only have 1 method named printArray.");
		}
	}

	public static <T> void printArray(T[] arr) {
		for (T t : arr) {
			System.out.println(t);
		}
	}
}