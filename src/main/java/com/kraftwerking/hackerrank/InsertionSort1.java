package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class InsertionSort1 {

	public static void insertIntoSorted(int[] ar) {
		// Fill up this function
		//printArray(ar);
		int[] tmp =ar.clone();
		
		for (int i = ar.length - 1; i >= 1; i--) {
			//System.out.println(i + " " + (i - 1));
			//System.out.println(ar[i] + " " + ar[i - 1]);
			if(ar[i]<ar[i-1]){
				tmp[i]=ar[i-1];
				printArray(tmp);
				
				tmp =ar.clone();
				tmp[i-1]=ar[i];
				tmp[i]=ar[i-1];
				ar[i]=tmp[i];
				ar[i-1]=tmp[i-1];
				tmp =ar.clone();
			}
		}
		printArray(ar);

	}

	/* Tail starts here */
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int s = in.nextInt();
		int[] ar = new int[s];
		for (int i = 0; i < s; i++) {
			ar[i] = in.nextInt();
		}
		insertIntoSorted(ar);
	}

	private static void printArray(int[] ar) {
		for (int n : ar) {
			System.out.print(n + " ");
		}
		System.out.println("");
	}

}
