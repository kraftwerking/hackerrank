package com.kraftwerking.hackerrank;

/**
 * Created by rmilitante on 2/14/17.
 */

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.io.PrintWriter;


public class LogfileParser {

    public static void main(String[] args) {
        try {
            File f = new File("/Users/rmilitante/Developer/workspace/Hackerrank/src/main/java/com/kraftwerking/hackerrank/eof.txt");
            Scanner sc = new Scanner(f);
            PrintWriter writer = new PrintWriter("/Users/rmilitante/Developer/workspace/Hackerrank/src/main/java/com/kraftwerking/hackerrank/eofout.txt", "UTF-8");

            //List<Person> people = new ArrayList<Person>();
            int count = 0;
            while(sc.hasNextLine()){
                String line = sc.nextLine();
                //System.out.println(line);
                count++;
                System.out.println(count);
                String[] details = line.split(" ");
                String det1 = details[0];
                String det2 = details[1];
                String det3 = details[2];
                String det4 = details[3];
                String det5 = details[4];
                //System.out.println(det1 + " " + det2 + " " + det3 + " " + det4 + " " + det5);
                line = line.replace(det1,"");
                line = line.replaceFirst(det2,"");
                line = line.replaceFirst(det3,"");
                line = line.replaceFirst(det4,"");
                line = line.replaceFirst(det5,"");
                line = line.trim();

                details = line.split(" ");
                det1 = details[0];

                if(Character.isDigit(det1.charAt(0))){
                    //System.out.println(det1);
                    line = line.replace(det1,"");
                    line = line.trim();
                }
                //System.out.println(line);
                writer.println(line);


            }
            writer.close();
            System.out.println("done");

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

