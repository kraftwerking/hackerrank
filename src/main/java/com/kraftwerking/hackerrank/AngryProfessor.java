package com.kraftwerking.hackerrank;

import java.util.Scanner;

public class AngryProfessor {

    public static void main(String[] args) {
        /* Enter your code here. Read input from STDIN. Print output to STDOUT. Your class should be named Solution. */
        Scanner sc = new Scanner(System.in);
        int t = sc.nextInt();

        for(int i=0;i<t;i++){
            int n = sc.nextInt();
            int k = sc.nextInt();
            int res = 0;
            for(int j=0;j<n;j++){
                int tm=sc.nextInt();
                if(tm<=0){
                    res++;
                }
            }

            if(res<k){
                System.out.println("YES");
            } else {
                System.out.println("NO");

            }

            //System.out.println("Hey check us out");
        }

    }


}