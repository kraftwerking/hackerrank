package com.kraftwerking.hackerrank;

import java.util.*;
import java.io.*;

class Noder {
	Noder left, right;
	int data;

	Noder(int data) {
		this.data = data;
		left = right = null;
	}
}

class BinaryTreeTopView {

	/*
	 * class Noder int data; Noder left; Noder right;
	 */
	/*
	   class Noder 
	       int data;
	       Noder left;
	       Noder right;
	*/
	void top_view(Noder root)
	{

	    if(root != null) {

	        top_view(root.left, true);

	        System.out.print(root.data + " ");

	        top_view(root.right, false);

	    }

	}

	void top_view(Noder node, boolean goLeft) {

	    if(node != null) {
	        if(goLeft) {
	            top_view(node.left, goLeft);
	            System.out.print(node.data + " ");
	        } else {
	            System.out.print(node.data + " ");
	            top_view(node.right, goLeft);
	        }
	    } 

	}
}