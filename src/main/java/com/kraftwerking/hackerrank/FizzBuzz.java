package com.kraftwerking.hackerrank;

import java.io.*;
import java.util.*;
import java.text.*;
import java.math.*;
import java.util.regex.*;

public class FizzBuzz {

	public static void main(String[] args) throws IOException {
		/*
		 * Enter your code here. Read input from STDIN. Print output to STDOUT.
		 * Your class should be named Solution.
		 */
		 Scanner scanner = new Scanner(System.in);
	        String str = scanner.nextLine();
	        int val = Integer.parseInt(str);
	        
	        for(int i=1;i<=val;i++){
		        if(i%3==0 && i%5==0){
		        	System.out.println("FizzBuzz");
		        } else if(i%3==0){
		        	System.out.println("Fizz");
		        } else if(i%5==0){
		        	System.out.println("Buzz");
		        } else {
		        	System.out.println(i);
		        }
		        
	        }
	       


	        
	}

}